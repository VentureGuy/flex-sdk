# 4.17.x Alpha

## May 18, 2019
* Added `IncludeDirectiveNode.original_filespec`, which exposes the filespec of `include` statements prior to pathfinding.

## May 16, 2019
* Fixed `kind=""` display in SyntaxTreeDumper.

## May 9, 2019
* Added is_each to ForStatementNode. See [this mailing list thread](http://mail-archives.apache.org/mod_mbox/flex-dev/201905.mbox/%3c6f3bb7da-031e-4bd5-6616-a62d1e0a9134@yahoo.com%3e).
